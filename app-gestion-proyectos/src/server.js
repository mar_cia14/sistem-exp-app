import sirv from "sirv";
// import polka from "polka";
import express from "express";
import compression from "compression";
import * as sapper from "@sapper/server";

const app = express();

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === "development";

/*Use de Express */

// const express = require("express");
// const path = require("path");

// app.use(express.static(path.join(__dirname, "public")));

// app.listen(8080, () => {
//   console.log("App started and available at http://localhost:8080");
// });

/** */

app.use(express.urlencoded());
app.use(express.json());

// const app = polka() // You can also use Express
app
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware()
  )
  .listen(PORT, (err) => {
    if (err) {
      console.log("error", err);
    }
  });

// export default app.handler; // Remove .handler when using Express

// if (!process.env.NOW_REGION) {
//   app.listen(PORT, (err) => {
//     if (err) console.log("error", err);
//   });
// }

// // server.js
// // import bodyParser from "body-parser";
// // app.use(
// //   bodyParser.urlencoded({
// //     extended: true,
// //   }),
// //   bodyParser.json()
// // );

//server.js
const formidableMiddleware = require("express-formidable");
app.use(formidableMiddleware());
