// routes/gateway/media_objects.js
const fetch = require("node-fetch");
var fs = require("fs");
var FormData = require("form-data");

export async function post(req, res, next) {
  // External API information.
  //const { API_SERVICE, API_KEY } = process.env;
  const { API_SERVICE, API_KEY } = "http://127.0.0.1:8000/api/";

  let formData = new FormData();
  formData.append(
    "file",
    fs.createReadStream(req.files.file.path),
    req.files.file.name
  );
  const response = await fetch(API_SERVICE + "/api/auth/store-file", {
    method: "POST",
    headers: {
      "API-KEY": API_KEY,
    },
    body: formData,
  });
  res.sendStatus(response.status);
}
